"""
Modulo de entidades da solucao em desenvolvimento.
Data de criacao: 01/01/2017
por: Wanderson Ferreira
"""
from database import Database
from sqlalchemy import Column, Integer, String, Date, Numeric, BIGINT

class Produto(Database().base):

    __tablename__ = 'produtos'
    
    id = Column(Integer, primary_key=True)
    data_consulta = Column(Date)
    produto_id = Column(BIGINT)
    nome = Column(String(50))
    categoria_nome = Column(String(50))
    categoria_id = Column(Integer)
    preco_max = Column(Numeric(5, 5))
    preco_min = Column(Numeric(5, 5))
    numero_ofertas = Column(Integer)
    numero_vendedores = Column(Integer)
    numero_comentarios = Column(Integer)
    rating = Column(Numeric(5, 5))


class ProdutoBuscape(object):
    """Documentacao da classe de Produto.

    """
    versao = '0.2'
        
    def __init__(self, produto_id):
        self.produto_id = produto_id
        self.consulta = self.__consultar_produto()
        self.categoria_nome, self.categoria_id = self.preencher_categoria()
        
    
    def __iniciar_conexao(self, ambiente_teste=True):
        from buscape import Buscape

        applicationID = '64377a346a2b38445850303d'
        buscape = Buscape(applicationID=applicationID, sandbox=ambiente_teste)
        return buscape
        

    def __consultar_produto(self):
        import json

        buscape = self.__iniciar_conexao()
        produto_request = buscape.view_product_details(productID=self.produto_id,
                                                       format=self.formato)
        dict_produto = json.loads(produto_request['data'])
        return dict_produto
        

    def __categoria_nome_id(self, dict_consulta):
        dict_categoria = dict_consulta['category']
        nome_categoria = dict_categoria['name']
        id_categoria = dict_categoria['id']
        return nome_categoria, id_categoria

    def preencher_categoria(self):
        nome_categoria, id_categoria = self.__categoria_nome_id(self.consulta)
        return nome_categoria, id_categoria


    def dicionario_produto(self):
        dict_produto = self.consulta['product'][0]['product']
        return dict_produto
    
    @property
    def formato(self):
        return 'json'
    
    @property
    def nome(self):
        dicionario_produto = self.dicionario_produto()
        return dicionario_produto['productname']

    @property
    def preco_max(self):
        dicionario_produto = self.dicionario_produto()
        pmax = float(dicionario_produto['pricemax'])
        return pmax

    @property
    def preco_min(self):
        dicionario_produto = self.dicionario_produto()
        pmin = float(dicionario_produto['pricemin'])
        return pmin

    @property
    def numero_ofertas(self):
        dicionario_produto = self.dicionario_produto()
        return dicionario_produto['numoffers']

    @property
    def ratings(self):
        dicionario_produto = self.dicionario_produto()
        dict_rating = dicionario_produto['rating']['useraveragerating']
        val_rating = float(dict_rating['rating'])
        return val_rating

    @property
    def numero_comentarios(self):
        dicionario_produto = self.dicionario_produto()
        dict_rating = dicionario_produto['rating']['useraveragerating']
        val_comments = dict_rating['numcomments']
        return val_comments

    @property
    def numero_vendedores(self):
        dicionario_produto = self.dicionario_produto()
        return dicionario_produto['totalsellers']

    @property
    def especificacoes(self):
        dicionario_produto = self.dicionario_produto()
        especs = dicionario_produto['specification']['item']
        dict_de_especificacoes = []
        
        for elemento in item[0]:
            label = item[0][el]['label']
            value = item[0][el]['value']
            dict_de_especificacoes[label] = value
        return dict_de_especificacoes
            

    
