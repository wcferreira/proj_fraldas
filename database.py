from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base


class Database(object):
    """Documentacao para a classe que lida com o banco de dados
    """
    
    def __init__(self, usuario='wanderson', senha='wanderson', hostname='localhost', database_name='projeto_fraldas'):
        self.usuario = usuario
        self.senha = senha
        self.hostname = hostname
        self.database_name = database_name
        self.dict_conexao = self.conectar()

    def conectar(self):
        from sqlalchemy import create_engine
        from sqlalchemy.orm import scoped_session, sessionmaker
        from sqlalchemy.ext.declarative import declarative_base

        engine = create_engine("mysql+pymysql://{}:{}@{}:3306/{}".format(self.usuario, self.senha, self.hostname, self.database_name))
        sessao = scoped_session(sessionmaker(bind=engine))
        Base = declarative_base()
        Base.query = sessao.query_property()
        Base.sessao = sessao

        return {'engine': engine, 'sessao': sessao, 'base': Base}

    @property
    def base(self):
        Base = self.dict_conexao.get('base', None)
        return Base

    @property
    def engine(self):
        engine = self.dict_conexao.get('engine', None)
        return engine

    @property
    def sessao(self):
        sessao = self.dict_conexao.get('sessao', None)
        return sessao
        
